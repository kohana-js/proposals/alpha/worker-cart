describe('worker', ()=>{

  const postRequest = {
    headers : new Headers({
      "accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
      "accept-language":"en-US,en;q=0.9",
      "cache-control":"no-cache",
      "content-type":"application/x-www-form-urlencoded",
      "pragma":"no-cache",
      "sec-fetch-dest":"document",
      "sec-fetch-mode":"navigate",
      "sec-fetch-site":"same-origin",
      "sec-fetch-user":"?1",
      "upgrade-insecure-requests":"1"
    }),
    referrer : "https://app.cart-steam.com/add",
    body : "sender=app.cart-steam.com.65554433.hashofthedomainandtimestamp&id=apple+macbook&quantity=4&properties%5Bnote%5D=foo-bar",
    method : "POST",
  };

  const postRequestNew = {
    "credentials":"include",
    "headers":{
      "accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
      "accept-language":"en-US,en;q=0.9",
      "cache-control":"no-cache",
      "content-type":"application/x-www-form-urlencoded",
      "pragma":"no-cache",
      "sec-fetch-dest":"document",
      "sec-fetch-mode":"navigate",
      "sec-fetch-site":"same-origin",
      "sec-fetch-user":"?1",
      "upgrade-insecure-requests":"1"
    },
    "referrer":"https://app.cart-steam.com/add",
    "referer":"https://app.cart-steam.com/add",
    "referrerPolicy":"no-referrer-when-downgrade",
    "body":"id=1111&quantity=123&properties%5Bnote%5D=foo-bar",
    "method":"POST",
    "mode":"cors"
  };

  const postRequestWithPayload = {
    "credentials":"include",
    "headers":{
      "accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
      "accept-language":"en-US,en;q=0.9",
      "cache-control":"no-cache",
      "content-type":"application/x-www-form-urlencoded",
      "pragma":"no-cache",
      "sec-fetch-dest":"document",
      "sec-fetch-mode":"navigate",
      "sec-fetch-site":"same-origin",
      "sec-fetch-user":"?1",
      "upgrade-insecure-requests":"1",
      "cookie": "__cfduid=d89198a817e88d0d4f5055ab768a4d9561583304975; sid=65195900-638d-11ea-ac86-854d6557b84a.241xU5Kt7tQ3pgJLr3CKEIbpqojCFYBcnBRZFGL2SUA; payload=%7B%22cart%22%3A%7B%22ct%22%3A1583942647468%2C%22apple%20macbook%22%3A%5B%7B%22p%22%3A%7B%22note%22%3A%22easter-sales%22%7D%2C%22q%22%3A3%7D%5D%2C%22ut%22%3A1583942647468%7D%7D.XAUXKsdj6BavxN8altfHTAQ9ogwjhqBX2xsSWHjJWcs"
    },
    "referrer":"https://app.cart-steam.com/add",
    "referrerPolicy":"no-referrer-when-downgrade",
    "body":"id=apple+macbook&quantity=3&properties%5Bnote%5D=easter-sales",
    "method":"POST",
    "mode":"cors"
  };

  const postRequestWithPayloadV2 = {
    "credentials":"include",
    "headers":{
      "accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
      "accept-language":"en-US,en;q=0.9",
      "cache-control":"no-cache",
      "content-type":"application/x-www-form-urlencoded",
      "pragma":"no-cache",
      "sec-fetch-dest":"document",
      "sec-fetch-mode":"navigate",
      "sec-fetch-site":"same-origin",
      "sec-fetch-user":"?1",
      "upgrade-insecure-requests":"1"
    },
    "referrer":"https://app.cart-steam.com/add",
    "referrerPolicy":"no-referrer-when-downgrade",
    "body":"sender=app.cart-steam.com.65554433.hashofthedomainandtimestamp&id=1111&quantity=123&properties%5Bnote%5D=foo-bar",
    "method":"POST",
    "mode":"cors"
  };

  const homeRequest = {
    "credentials":"include",
    "headers": {
      "accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
      "accept-language":"en-US,en;q=0.9",
      "cache-control":"no-cache",
      "pragma":"no-cache",
      "sec-fetch-dest":"document",
      "sec-fetch-mode":"navigate",
      "sec-fetch-site":"none",
      "sec-fetch-user":"?1",
      "upgrade-insecure-requests":"1"
    },
    "referrerPolicy":"no-referrer-when-downgrade",
    "body":null,
    "method":"GET",
    "mode":"cors"
  };

  const addRequest = {
    "credentials":"include",
    "headers": {
      "accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
      "accept-language":"en-US,en;q=0.9",
      "cache-control":"no-cache",
      "pragma":"no-cache",
      "sec-fetch-dest":"document",
      "sec-fetch-mode":"navigate",
      "sec-fetch-site":"none",
      "sec-fetch-user":"?1",
      "upgrade-insecure-requests":"1"
    },
    "referrerPolicy":"no-referrer-when-downgrade",
    "body":null,
    "method":"GET",
    "mode":"cors"
  };

  const requestUpdateCartV2 = {
    "credentials":"include",
    "headers":{
      "accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
      "accept-language":"en-US,en;q=0.9",
      "cache-control":"no-cache",
      "content-type":"application/x-www-form-urlencoded",
      "pragma":"no-cache",
      "sec-fetch-dest":"document",
      "sec-fetch-mode":"navigate",
      "sec-fetch-site":"same-origin",
      "sec-fetch-user":"?1",
      "upgrade-insecure-requests":"1",
      "cookie": "sid=d9f01f40-656b-11ea-b7bb-4998c9459702.SME3mkmdnZygIvyF65%2Fmw3DoFUw6EMzBv2F2aa5E5R0; payload=%7B%22carts%22%3A%7B%22ct%22%3A1584133168268%2C%22app.cart-steam.com%22%3A%7B%221111%22%3A%5B%7B%22p%22%3A%7B%22note%22%3A%22foo-bar%22%7D%2C%22q%22%3A984%7D%5D%2C%22ut%22%3A1584133191998%7D%7D%7D.2ZCUvYcB6Uj0t5lS7oIOfov97j9IWFEsf%2BUu0SF2nNU; __cfduid=d627a4f42a53d6d4fc32468be581be8111583944569"
    },
    "referrer":"https://app.cart-steam.com/add",
    "referrerPolicy":"no-referrer-when-downgrade",
    "body":"id=22222&quantity=123&properties%5Bnote%5D=foo-bar",
    "method":"POST",
    "mode":"cors",
  };

  const requestAddCartJSON = {
    "credentials":"include",
    "headers":{
      "accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
      "accept-language":"en-US,en;q=0.9",
      "cache-control":"no-cache",
      "content-type":"application/json",
      "pragma":"no-cache",
      "sec-fetch-dest":"document",
      "sec-fetch-mode":"navigate",
      "sec-fetch-site":"same-origin",
      "sec-fetch-user":"?1",
      "upgrade-insecure-requests":"1",
      "cookie": "sid=d9f01f40-656b-11ea-b7bb-4998c9459702.SME3mkmdnZygIvyF65%2Fmw3DoFUw6EMzBv2F2aa5E5R0"
    },
    "referrer":"https://app.cart-steam.com/add",
    "referrerPolicy":"no-referrer-when-downgrade",
    "body" : null,
    "method":"POST",
    "mode":"cors",
  };

  const requestClearCart = {
    "credentials":"include",
    "headers":{
      "accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
      "accept-language":"en-US,en;q=0.9",
      "cache-control":"no-cache",
      "content-type":"application/x-www-form-urlencoded",
      "pragma":"no-cache",
      "sec-fetch-dest":"document",
      "sec-fetch-mode":"navigate",
      "sec-fetch-site":"same-origin",
      "sec-fetch-user":"?1",
      "upgrade-insecure-requests":"1",
      "cookie": "sid=d9f01f40-656b-11ea-b7bb-4998c9459702.SME3mkmdnZygIvyF65%2Fmw3DoFUw6EMzBv2F2aa5E5R0; payload=%7B%22carts%22%3A%7B%22ct%22%3A1584133168268%2C%22app.cart-steam.com%22%3A%7B%221111%22%3A%5B%7B%22p%22%3A%7B%22note%22%3A%22foo-bar%22%7D%2C%22q%22%3A984%7D%5D%2C%22ut%22%3A1584133191998%7D%7D%7D.2ZCUvYcB6Uj0t5lS7oIOfov97j9IWFEsf%2BUu0SF2nNU; __cfduid=d627a4f42a53d6d4fc32468be581be8111583944569"
    },
    "method":"GET",
    "mode":"cors",
  };

  const requestAddCartV2NoQtyNoProp = {
    "credentials":"include",
    "headers":{
      "accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
      "accept-language":"en-US,en;q=0.9",
      "cache-control":"no-cache",
      "content-type":"application/x-www-form-urlencoded",
      "pragma":"no-cache",
      "sec-fetch-dest":"document",
      "sec-fetch-mode":"navigate",
      "sec-fetch-site":"same-origin",
      "sec-fetch-user":"?1",
      "upgrade-insecure-requests":"1",
      "cookie": "sid=d9f01f40-656b-11ea-b7bb-4998c9459702.SME3mkmdnZygIvyF65%2Fmw3DoFUw6EMzBv2F2aa5E5R0; payload=%7B%22carts%22%3A%7B%22ct%22%3A1584133168268%2C%22app.cart-steam.com%22%3A%7B%221111%22%3A%5B%7B%22p%22%3A%7B%22note%22%3A%22foo-bar%22%7D%2C%22q%22%3A984%7D%5D%2C%22ut%22%3A1584133191998%7D%7D%7D.2ZCUvYcB6Uj0t5lS7oIOfov97j9IWFEsf%2BUu0SF2nNU; __cfduid=d627a4f42a53d6d4fc32468be581be8111583944569"
    },
    "referrer":"https://app.cart-steam.com/add",
    "referrerPolicy":"no-referrer-when-downgrade",
    "body":"id=22222",
    "method":"POST",
    "mode":"cors",
  };

  const {handleRequest} = require('../source/index');
  const {buildRequest} = require('cloudflare-worker-test-utils');
  const qs = require('qs');
  const eq = require('fast-deep-equal');

  global.KVSTORE = {
    get: async(key)=>'mock-value',
    put : async(key, value)=>'mock-set-value',
    list: async(opts) => [],
    delete : async(key) => {},
  };

  test('test eq', ()=>{
    expect(eq({}, {})).toBe(true);
  });

  test('home', async () => {
    const response = await handleRequest(buildRequest("https://app.cart-steam.com/", homeRequest));
    expect(response.status).toBe(200);
    expect(await response.text()).toBe('{"hello":"world!!"}');
  });

  test('add', async () => {
    const response = await handleRequest(buildRequest(  "https://app.cart-steam.com/add", addRequest));
    expect(response.status).toBe(200);
  });

  test('new submit', async () => {
    const response = await handleRequest(buildRequest("https://app.cart-steam.com/add", postRequest));
    if(response.status === 500){
      console.log(await response.text());
    }
    expect(response.status).toBe(200);
  });

  test('new submit with domain', async () => {
    const response = await handleRequest(buildRequest("https://app.cart-steam.com/app.cart-steam.com/add", postRequestNew));
//    expect(await response.text()).toBe('');
    expect(response.status).toBe(200);
  });

  test('submit invalid cookie sign', async () => {
    const request = buildRequest("https://app.cart-steam.com/add", postRequestWithPayload);
    const response = await handleRequest(request);
    expect(response.status).toBe(403);
  });

  test('check-referral', async () => {
    const rawRequest = postRequestWithPayloadV2;
    expect(rawRequest.method).toBe('POST');
    const request = buildRequest("https://app.cart-steam.com/app.cart-steam.com/add", rawRequest);
    expect(request.method).toBe('POST');
    expect(request.referrer).toBe('https://app.cart-steam.com/add');
    const response = await handleRequest(request);
    expect(response.status).toBe(200);
  });

  test('check-referral-reject', async () => {
    const request = buildRequest("https://app.cart-steam.com/app.dim-my.com/add", postRequestWithPayloadV2);
    expect(request.method).toBe('POST');
    expect(request.referrer).toBe('https://app.cart-steam.com/add');
    const response = await handleRequest(request);
    expect(response.status).toBe(403);
    expect(await response.text()).toBe('403 / Bot behaviour detected.');
  });

  test('update cart v2', async ()=>{
    const request = buildRequest("https://app.cart-steam.com/app.cart-steam.com/add", requestUpdateCartV2);
    expect(request.method).toBe('POST');
    expect(request.referrer).toBe('https://app.cart-steam.com/add');
    const response = await handleRequest(request);
    expect(response.status).toBe(200);
  });

  test('JSON add cart', async ()=>{
    const req = requestAddCartJSON;
    req.body = JSON.stringify({
      items:[
        {quantity:5, id: 111},
        {quantity:2, id: 222},
        {quantity:2, id: 111},
        {quantity:3, id: 111, properties:{note: "foo-bar"}}
      ]
    });

    const request = buildRequest("https://app.cart-steam.com/app.cart-steam.com/add", req);
    expect(request.method).toBe('POST');
    expect(request.referrer).toBe('https://app.cart-steam.com/add');
    const response = await handleRequest(request);
    expect(response.status).toBe(200);
    const result = JSON.parse( await response.text() );

    const setCookie = qs.parse(response.headers.get('Set-Cookie'));
    const cookiePartials = setCookie.payload.split('; ');

    const cookie = JSON.parse(cookiePartials[0].replace(/.[^.]+$/, ''));

    expect(cookiePartials[1]).toBe('Max-Age=604800');
    expect(cookiePartials[2]).toBe('Path=/');
    expect(cookiePartials[3]).toBe('HttpOnly');
    expect(cookie.carts['app.cart-steam.com']['111'][0][1]).toBe(7);
    expect(cookie.carts['app.cart-steam.com']['111'][1][1]).toBe(3);
    expect(cookie.carts['app.cart-steam.com']['222'][0][1]).toBe(2);

    expect(result.carts['app.cart-steam.com']['111'][0][1]).toBe(7);
    expect(result.carts['app.cart-steam.com']['111'][1][1]).toBe(3);
    expect(result.carts['app.cart-steam.com']['222'][0][1]).toBe(2);

    expect(JSON.stringify(result.carts['app.cart-steam.com']['111'][0][0])).toBe(JSON.stringify({}));
    expect(JSON.stringify(result.carts['app.cart-steam.com']['111'][1][0])).toBe(JSON.stringify({note: 'foo-bar'}));

  });

  test('clear cart', async ()=>{
    const request = buildRequest("https://app.cart-steam.com/clear", requestClearCart);
    expect(request.method).toBe('GET');
    const response = await handleRequest(request);

    expect(await response.text()).toBe(JSON.stringify({type: 'CLEAR', payload: {success: true}}))
    expect(response.status).toBe(200);
  });

  test('add without quantity', async ()=>{
    const request = buildRequest("https://app.cart-steam.com/app.cart-steam.com/add", requestAddCartV2NoQtyNoProp);
    expect(request.method).toBe('POST');
    expect(request.referrer).toBe('https://app.cart-steam.com/add');
    const response = await handleRequest(request);
    expect(response.status).toBe(200);
  })
});