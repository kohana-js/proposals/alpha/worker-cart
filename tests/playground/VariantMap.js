const id = 20_039_580_241_412_003_958_024_141;
//console.log(id.toString(2).length);
//console.log(0b1_11010010_10010101_00110100_11000111_11001101);

//11001101
//11000111
//00110100
//10010101
//11111111

//const buffer = new ArrayBuffer(8);
//const header = new DataView(buffer);
//const headerByteSize = 8;
//let bits = BigInt(0b1_1101_0010_1001_0101_0011_0100_1100_0111_1100_1101);
//let ver = BigInt(0b1010_1111_1111_0000_0000_1111_1111_0000_0000);
//console.log('1000010010011100010111101001001011001000101011010110100000001010111101110011111001101'.length);
//console.log('1000010010011100010111101001001011001000101011010110100000001010111101110011111001101');
//console.log(ver);
//console.log(bits);
/*
for(let offset = (headerByteSize - 2); offset >= 0 ; offset--){
  const value = bits & 255;
  header.setUint8(offset, value);
  bits = bits >> 8;

  console.log(value.toString(2));
}
*/

//console.log(20_039_580_241_412_003_958_024_141n.toString(2));

const buffer = new ArrayBuffer(20);
const data = new DataView(buffer);
const value = 0b11110000_11001111_11010011_10001000_10100100_10100011_10100011_00111100_01100110_11001111_10001001_01001000_10101010_00110000_00000000_00000000n;
data.setBigUint64(0, 0b11110000_11001111_11010011_10001000_10100100_10100011_10100011_00111100n);
data.setBigUint64(8, 0b01100110_11001111_10001001_01001000_10101010_00110000_00000000_00000000n);

const ceilDivide = (x, y) => (x + y - 1n) / y;
/**
 * private method getByte;
 *
 * @param {DataView} data
 * @param {BigInt} idx
 * @param {BigInt} size
 * @return *
 *
 */
const write = (data, idx, size) => {
  const startByte = idx * size / 8n;
  const endByte = ceilDivide((idx + 1n) * size , 8n);

  let result = BigInt(0);
  for(let i = startByte; i < endByte; i++ ){
    const segment = BigInt(data.getUint8(parseInt(i)));
    result = result << 8n | segment;
  }

  const maskShift = (endByte * 8n) - ((idx + 1n) * size);

  const resultSize = (endByte - startByte) * 8n;
  const mask = (2n ** resultSize - 1n) ^ (((2n ** size - 1n)) << maskShift);

  console.log(startByte, endByte, result.toString(2), mask.toString(2).padStart(parseInt(resultSize), '0'));
  console.log((result & mask).toString(2));



/*
  const offsetBit = idx * size;
  const offsetByte = offsetBit / 8n;
  const shiftBit   = offsetBit % 8n;

  const frameByteSize = ceilDivide(size, 8n);
  const frameSize     = frameByteSize * 8n;
  const valueOffset   = (frameSize - size - shiftBit);

  let result = BigInt(0);

  for(let i = 0n; i < frameByteSize; i++ ){
    const segment = BigInt(data.getUint8(parseInt(offsetByte + i)));
//    console.log(segment.toString(2).padStart(8, '0'));
    result = result << 8n | segment;
  }

  const mask = ((2n ** size) - 1n) ;
  console.log(result.toString(2).padStart(8, ''));
  console.log(valueOffset);*/
};

'0b11110000_11001111_11010011_10001000_10100100_10100011_10100011_00111100_01100110_11001111_10001001_01001000_10101010_00110000_00000000_00000000n';
'0b.11110.000_11.00111.1_1101.0011_1.00010.00_10100100_10100011_10100011_00111100_01100110_11001111_10001001_01001000_10101010_00110000_00000000_00000000n';

write(data, 0n, 5n);//11110 0,1 0 > 0           << 3  0    0   8-5
write(data, 1n, 5n);//00011 0,2 0 - 678 1 - 12  << 6  5    5   16-10
write(data, 2n, 5n);//00111 1,2 1 - 34567       << 1  10   2   16-15
write(data, 3n, 5n);//11101 1,3 1 - 8 2 - 1234  << 4  15   7   24-20
write(data, 4n, 5n);//00111 2,4 2 - 5678 3 - 1  << 7  20   4   32-15
write(data, 5n, 5n);//00111 3,4 3 - 23456       << 2  25   1

const m2 = (0b1111_1111_1111_1111n ^ 0b1111_0000n);
console.log(m2.toString(2));
/*
1111 1111 1111 1111
0000 0000 1111 0000

1111 1111 0000 1111



     11110000
mask 11111000
OR   00000000
XOR  00000000

AND

targ 00000111
*/

/*
console.log(ceilDivide(1n, 8n));
console.log(ceilDivide(5n, 8n));
console.log(ceilDivide(8n, 8n));
console.log(ceilDivide(9n, 8n));

const offset = 3;
const bitSize = 40;
let result = BigInt(0);
for(let i = 0; i < Math.ceil(bitSize / 8); i++ ){
  const segment = BigInt(data.getUint8(offset + i));
  console.log(segment.toString(2).padStart(8, '0'));
  result = result << 8n | segment;
}

console.log(value);
console.log(0b10001000_10100100_10100011_10100011_00111100n.toString(2));
console.log(result.toString(2));

console.log(8n/8n * 8n);*/