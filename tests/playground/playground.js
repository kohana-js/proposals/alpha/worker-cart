require('@komino/cloudflare-worker-test-utils');
const {encode, decode} = require('base64-arraybuffer');

const bufferToBase64 = buffer => {
  return btoa(String.fromCharCode(...new Uint8Array(buffer)));
};

const sign = async(str, jwk) =>{
  const key = await crypto.subtle.importKey(
    "jwk",
    jwk,
    {name:"HMAC","hash":"SHA-256"},
    true,
    ["sign", "verify"]);

  const buffer = new TextEncoder().encode(str);
  return await crypto.subtle.sign("HMAC", key, buffer);
};

const jwk = {
  "alg":"HS256",
  "ext":true,
  "k":"TnLSCzt8Kd5i5PGz0FjudvapySVyxN7YQ_3FFPBXsE5AX5nARMAzHO1eL2kxpwx4QKgxsHnY80xl9O5NUatsag",
  "key_ops":["sign","verify"],
  "kty":"oct"
};

sign('fe8d4800-644e-11ea-9393-d36414dc0c99', jwk).then(x => {
  const y = bufferToBase64(x);//.replace(/\=+$/, '');
  const w = encode(x);
  const z = decode(y);

  console.log('sign', x, w, y, z);
});

//'fe8d4800-644e-11ea-9393-d36414dc0c99.WqZL4g7JYuXgHllP/ZfGTW9zDnr3AweKiL9OGt7Vupw';

//console.log(atob('WqZL4g7JYuXgHllP/ZfGTW9zDnr3AweKiL9OGt7Vupw').toString('base64'));

const keyGen = async () =>{
  const k = await crypto.subtle.generateKey(
    {name:"HMAC","hash":"SHA-256"},
    true,
    ["sign", "verify"]
  );
  return bufferToBase64(await crypto.subtle.exportKey('raw', k));
};

keyGen().then(x => console.log('key', x));
