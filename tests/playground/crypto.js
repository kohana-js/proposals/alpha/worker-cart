require('@komino/cloudflare-worker-test-utils');
const {encode, decode} = require('base64-arraybuffer');

const jwk = {
  "alg":"HS256",
  "ext":true,
  "k":"TnLSCzt8Kd5i5PGz0FjudvapySVyxN7YQ_3FFPBXsE5AX5nARMAzHO1eL2kxpwx4QKgxsHnY80xl9O5NUatsag",
  "key_ops":["sign","verify"],
  "kty":"oct"
};

const rk = "TnLSCzt8Kd5i5PGz0FjudvapySVyxN7YQ/3FFPBXsE5AX5nARMAzHO1eL2kxpwx4QKgxsHnY80xl9O5NUatsag==";
const main = async () => {
  const key = await crypto.subtle.importKey('jwk', jwk,{name:"HMAC","hash":"SHA-256"}, true, ["sign", "verify"]);
  const raw = await crypto.subtle.exportKey('raw', key);
  const base64raw = encode(raw);
  console.log(base64raw);

  const key2 = await crypto.subtle.importKey('raw', decode(base64raw), {name:"HMAC","hash":"SHA-256"}, true, ["sign", "verify"]);

  const buffer = new TextEncoder().encode('Hello my friend Peter Pan');
  const sign1 = await crypto.subtle.sign("HMAC", key, buffer);
  const sign2 = await crypto.subtle.sign("HMAC", key2, buffer);

  console.log(encode(sign1), encode(sign2));
};

main();