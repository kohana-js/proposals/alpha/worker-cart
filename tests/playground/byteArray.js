require('@komino/cloudflare-worker-test-utils');

//    const mask = [0, 0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xFE, 0xFF];
//[0, 1, 3, 7, 15, 31, 63, 127, 255];
//    const mask = new Uint32Array(1);

const QTY_HEADER = 0x30;
const VERSION = QTY_HEADER >> 5;
const SIZE = QTY_HEADER & 0x1F;
const MAX_QTY = 2 ** SIZE;
const MAX_PRODUCT = 3;

//console.log(VERSION);
//console.log(SIZE);
//console.log(MAX_QTY);

//const buffer = new ArrayBuffer(1 + 2 * MAX_PRODUCT);
//const data = new DataView(buffer);
//data.setInt8(0, QTY_HEADER);
//data.setInt16(1, 255);

//console.log(data.getInt16(1));


//const bytes = new Uint8Array(buffer);
//const bin = InventoryFile.toBinaryString(bytes);
//console.log(bin);

//console.log(encode(buffer));


//console.log(1 << 5 | 16);
//console.log(0x30);

//const f = InventoryFile.parse('MAD/AAAAAA==');
//console.log(f.version, f.size, f.maxQty);

const f2 = new InventoryFile(1, 10, 2);
console.log( InventoryFile.toBinaryString(new Uint8Array(f2.buffer)) );
f2.write(0, 1023);
console.log( InventoryFile.toBinaryString(new Uint8Array(f2.buffer, 1), f2.size));

f2.write(1, 1023);
console.log( InventoryFile.toBinaryString(new Uint8Array(f2.buffer, 1), f2.size));

console.log('end');