const qs = require('qs');
const cookie = require('cookie');
const cookieUtils = require('../source/classes/CookieUtils');


describe('cookie', () => {
  test('parse cookie', async () => {
    const raw = `__cfduid=d89198a817e88d0d4f5055ab768a4d9561583304975; sid=e3ae9730-611c-11ea-b3a4-4d4e44aeae0c.jV5JIknQFb%2FoMz6gmXUQFw4Rz%2BAtbcRzWSzdxTzXCfQ; payload=%7B%22cart%22%3A%7B%221111%22%3A%5B%7B%22p%22%3A%7B%22note%22%3A%22foo-bar%22%7D%2C%22q%22%3A123%7D%2C%7B%22p%22%3A%7B%22note%22%3A%22tata%22%7D%2C%22q%22%3A123%7D%5D%2C%22ct%22%3A1583658729507%2C%22ut%22%3A1583658752374%7D%7D.NFYoGN7zeR3Yj3pE%2B349jQxGJtB77UHVNffkECxKBMk`;
    const parsed = cookie.parse(raw);
    expect(parsed.__cfduid).toBe('d89198a817e88d0d4f5055ab768a4d9561583304975');
  });

  test('parse cookie II', async ()=>{
    const raw = `__cfduid=d89198a817e88d0d4f5055ab768a4d9561583304975; sid=e3ae9730-611c-11ea-b3a4-4d4e44aeae0c.jV5JIknQFb%2FoMz6gmXUQFw4Rz%2BAtbcRzWSzdxTzXCfQ; payload=%7B%22cart%22%3A%7B%221111%22%3A%5B%7B%22p%22%3A%7B%22note%22%3A%22foo-bar%22%7D%2C%22q%22%3A123%7D%2C%7B%22p%22%3A%7B%22note%22%3A%22tata%22%7D%2C%22q%22%3A123%7D%5D%2C%22ct%22%3A1583658729507%2C%22ut%22%3A1583658752374%7D%7D.NFYoGN7zeR3Yj3pE%2B349jQxGJtB77UHVNffkECxKBMk`;
    const parsed = cookieUtils.parseCookie(raw);
    expect(parsed.__cfduid).toBe('d89198a817e88d0d4f5055ab768a4d9561583304975');
  });

  test('parse cookie III', async () => {
    const raw = `__cfduid=d89198a817e88d0d4f5055ab768a4d9561583304975; sid=e3ae9730-611c-11ea-b3a4-4d4e44aeae0c.jV5JIknQFb%2FoMz6gmXUQFw4Rz%2BAtbcRzWSzdxTzXCfQ; payload=%7B%22cart%22%3A%7B%221111%22%3A%5B%7B%22p%22%3A%7B%22note%22%3A%22foo-bar%22%7D%2C%22q%22%3A123%7D%2C%7B%22p%22%3A%7B%22note%22%3A%22tata%22%7D%2C%22q%22%3A123%7D%5D%2C%22ct%22%3A1583658729507%2C%22ut%22%3A1583658752374%7D%7D.NFYoGN7zeR3Yj3pE%2B349jQxGJtB77UHVNffkECxKBMk`;
    const parsed = cookie.parse(raw, {decode : a => decodeURIComponent(a.replace(/\.[^.]+$/,''))});
    expect(parsed.sid).toBe('e3ae9730-611c-11ea-b3a4-4d4e44aeae0c');
    expect(parsed.__cfduid).toBe('d89198a817e88d0d4f5055ab768a4d9561583304975');

  });

});