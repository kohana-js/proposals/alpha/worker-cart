//https://stackoverflow.com/questions/39483893/how-can-i-use-my-webpacks-html-loader-imports-in-jest-tests

const htmlLoader = require('html-loader');

module.exports = {
  process(src, filename, config, options) {
    return htmlLoader(src);
  }
}