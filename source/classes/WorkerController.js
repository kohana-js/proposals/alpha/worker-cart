//https://developer.mozilla.org/en-US/docs/Web/API/Headers/Headers

const cookie = require('cookie');

module.exports = {
  handlerFactory : (Controller, options= {} ) => async request => {
    const c = new Controller(request);
    const result = await c.execute(options.action);

    result.headers['x-debug'] = JSON.stringify([...request.headers]);
    result.headers['x-url'] = request.url;
    result.headers['Content-type'] = result.headers['Content-type'] || 'text/html; charset=utf-8';

    const headers = new Headers(result.headers);
    result.cookies.forEach(x => {
      //{name, value, options}
      const name = x.name;
      const value = x.value;
      const options = Object.assign( {maxAge : 604800, httpOnly: true}, x.options);

      headers.append('Set-Cookie', cookie.serialize(name, value, options))
    });

    if(result.status === 500){
      result.body = result.body + '\n'+ c.error.stack;
    }

    return new Response(
      (typeof result.body === 'string') ? result.body : JSON.stringify(result.body),
      {headers : headers, status : result.status}
      );
  },
};