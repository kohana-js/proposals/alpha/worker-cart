const {encode, decode} = require('base64-arraybuffer');
const {BinaryStore} = require('binary-store');

class VariantMapFile extends BinaryStore{
  constructor(version= 1, dataSize=16, values = []) {
    super(version, 5, dataSize, values);
  }

  static decode(base64){
    const buffer = decode(base64);
    const data = new DataView( buffer );

    const result = new VariantMapFile( data.getUint32(0), data.getUint8(4));

    result.setBuffer(buffer);

    return result;
  }

  write(idx, value){
    super.write(idx, value);
    /* 1 11010010 10010101 00110100 11000111 11001101 */
  }

  encode(){
    return encode(this.buffer);
  }
}

Object.freeze(VariantMapFile);
Object.freeze(VariantMapFile.prototype);
module.exports = VariantMapFile;