const {Controller} = require('@kohanajs/core-mvc');

class ControllerFetch extends Controller{
  async action_fetch(){
    const res = await fetch('https://dev.shop-steam.com/');
    const text = await res.text();
    this.headers['Content-type'] = 'application/json; charset=utf-8';
    this.body = JSON.stringify(text);
  }
}

module.exports = ControllerFetch;