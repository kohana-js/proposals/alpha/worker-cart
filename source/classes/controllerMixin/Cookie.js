const {ControllerMixin} = require('@kohanajs/core-mvc');
const cookieUtils = require('../CookieUtils');
const uuid = require('uuid').v1;
const {decode} = require('base64-arraybuffer');

module.exports = class ControllerMixinCookie extends ControllerMixin{
  cookie = null;
  payload = null;

  constructor(client, key, options={}) {
    super(client);
    this.key = key;
    this.payloadMaxSize = options.payloadMaxSize || 65536;

    this.exports = {
      addCookie : (name, value, options) => this.addCookie(name, value, options),
      addSecureCookie : async (name, value, options) => await this.addSecureCookie(name, value, options),
      cookie: () => this.cookie,
      payload: ()=> this.payload,
    }
  }

  async before(){
    this.key = await crypto.subtle.importKey('raw', decode(this.key), {name:"HMAC","hash":"SHA-256"}, true, ["sign", "verify"]);
  }

  async addSecureCookie(name, value, options = null){
    const str = await cookieUtils.sign(value, this.key);
    this.addCookie(name, str , options);
  }

  addCookie(name, value, options= null){
    this.client.cookies.push({
      name: name,
      value: value,
      options : options || {maxAge: 604800, httpOnly: true, path: '/'},
    })
  }

  async verifyCookie(requestCookie = {}){
      //assign sid if not exist in cookie
      if ( !requestCookie.sid ) {
        await this.addSecureCookie('sid', uuid());
      }else{
        //verify sid
        //reject forge cookie;
        if(await cookieUtils.unsign(requestCookie.sid, this.key) === false){
          return this.client.forbidden();
        }
      }

      if( requestCookie.payload ) {
        if( requestCookie.payload.length > this.payloadMaxSize){
          return this.client.serverError(new Error(`cookie payload size over ${this.payloadMaxSize} byte`));
        }

        if(await cookieUtils.unsign(requestCookie.payload, this.key) === false){
          return this.client.forbidden();
        }
      }

  }

  async action_post_add(){
    const rawCookie = this.client.request.headers.get('cookie') || '';
    const cookie = cookieUtils.parseCookie(rawCookie || '');
    await this.verifyCookie(cookie);

    //remove cookie signature
    this.cookie = cookie;
    this.payload = JSON.parse(await cookieUtils.unsign(cookie.payload) || "{}");
  }

  async action_clear(){
    this.addCookie('sid', '', { httpOnly: true, expires: new Date(0)});
    this.addCookie('payload', '', { httpOnly: true, expires: new Date(0)});
  }
};