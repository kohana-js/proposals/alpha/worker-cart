const {ControllerMixin} = require('@kohanajs/core-mvc');

module.exports = class ControllerMixinCloudFlareKV extends ControllerMixin{
  constructor(client) {
    super(client);

    this.exports = {
      getKV : async (key) => await KVSTORE.get(key),
      setKV : async (key, value) => await KVSTORE.put(key, value),
    }
  }

  async action_post_add() {
    this.client.headers['KV'] = await KVSTORE.get('first-key');
  }
};