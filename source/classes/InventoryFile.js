const {encode, decode} = require('base64-arraybuffer');
const {BinaryStore} = require('binary-store');

class InventoryFile extends BinaryStore{
  constructor(version= 1, dataSize=16, values = []) {
    super(version, 5, dataSize, values);
  }

  static decode(base64){
    const buffer = decode(base64);
    const data = new DataView( buffer );

    const result = new InventoryFile( data.getUint32(0), data.getUint8(4));

    result.setBuffer(buffer);

    return result;
  }

  encode(){
    return encode(this.buffer);
  }


}

Object.freeze(InventoryFile);
Object.freeze(InventoryFile.prototype);
module.exports = InventoryFile;