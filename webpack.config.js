module.exports = {
  "mode": "production",
  "target": "webworker",
  "entry": "./source/index.js",
  module: {
    rules: [
      {
        test: /\.html$/i,
        loader: 'html-loader',
      },
    ],
  },
};